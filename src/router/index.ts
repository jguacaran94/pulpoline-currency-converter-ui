import { createRouter, createWebHistory } from 'vue-router'
import Converter from '@/components/Converter.vue'
import Logs from '@/components/Logs.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: '',
      component: Converter
    },
    {
      path: '/',
      name: 'logs',
      component: Logs
    },
    // {
    //   path: '/about',
    //   name: 'about',
    //   // route level code-splitting
    //   // this generates a separate chunk (About.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import('../views/AboutView.vue')
    // }
  ]
})

export default router
